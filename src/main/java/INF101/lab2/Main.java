package INF101.lab2;

import INF101.lab2.pokemon.IPokemon;
import INF101.lab2.pokemon.Pokemon;

public class Main {

    public static IPokemon pokemon1;
    public static IPokemon pokemon2;
    public static void main(String[] args) {
        // Have two pokemon fight until one is defeated

        pokemon1 = new Pokemon("Pikachu");
        pokemon2 = new Pokemon("Oddish");

        
        System.out.println(pokemon1 + " HP: (" + pokemon1.getCurrentHP() + "/" + pokemon1.getMaxHP() + ") STR: " + pokemon1.getStrength());
        System.out.println(pokemon2 + " HP: (" + pokemon2.getCurrentHP() + "/" + pokemon2.getMaxHP() + ") STR: " + pokemon2.getStrength());

        while (true) {
            pokemon1.attack(pokemon2);
            if (!pokemon2.isAlive()) break;
            pokemon2.attack(pokemon1);
            if (!pokemon1.isAlive()) break;
        }
    }
}
