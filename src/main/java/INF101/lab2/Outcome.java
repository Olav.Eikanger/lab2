package INF101.lab2;

import INF101.lab2.pokemon.IPokemon;

public class Outcome {
    private IPokemon winner;
    private IPokemon loser;

    public Outcome(IPokemon winner, IPokemon loser) {
        this.winner = winner;
        this.loser = loser;
    }

    public IPokemon getWinner() {
        return winner;
    }

    public IPokemon getLoser() {
        return loser;
    }
}
